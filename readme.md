Chronos
=============

##1. What is “Chronos”?
_Chronos_ is codename for Leaves tracking API that keeps track of user leaves request - leave time keeping.

##2. Technologies
It’s using PHP 5.4 as language of choice, with very flexible framework - Laravel.
Application is supported by mySQL database.

1. **PHP** - Must have :)
2. **mySQL** - The most popular open source RDBMS (especially for web applications). Can’t go wrong with this one...
3. **Laravel & Eloquent** - Laravel is quite non-invasive and very helpful when it comes to building REST API server.
Comes with a lot of helpers, and Eloquent ORM it works fantastic with MySQL.
Beside ORM, this application heavily relies on Laravel’s routing system and database schema manipulation (migrations / seeds).

### Database Schema
![eer.png](https://bitbucket.org/repo/rAzK4K/images/4194148085-eer.png)

##3. Postman collection
Chronos is closed API, which means that you’ll need username and password to login.  
To browse it, you need to use REST client. I recommend using [Postman](http://www.getpostman.com/).  
You can get Postman collection [here](https://www.dropbox.com/s/khbgopd0b3ye85l/Leaves%2520Request%2520System.json.postman_collection) .  

**User**  
**Username:** user  
**Password:** password


**Admin**  
**Username:** user  
**Password:** password

##4. Structure walkthrough
+ **.idea** - PHPStorm is recommended IDE
+ **_documents** - Documents, readmes, postman collections …
+ **app/** - application code - main part. Here you can find various settings, routes definitions, authentication filters. Most of the code is contained in Controllers and Models. Views are rarely used (only for root page).
Migrations and table seeds are located in database folder.
+ **models/rule** contains rules definitions.
+ **public/** - mostly static files but also includes API  document generator. When you update routes.php, run via console:
        php generate-api-docs.php
from root directory. Make sure that public/documentation.html is writable (chmod 777).

##5. Installation
1.	Pull package from repository.
2.	Run `composer update`
3.	Do `php artisan serve`

You’ve now started local PHP server, probably on localhost:8000 .  
Create a mySQL database, and configure `app/config/database.php` .  
Default db name is: _leavessystem_ .  
After database is created, you need to run migrations and seed tables.  
Run from terminal:
```
        php artisan migrate
        php artisan db:seed
```
You should see status message saying that tables are seeded.
You can now run Postman and import collection list.

**Migrations** are files that manage database schema and they’re located under `app/database/migrations`.  
You can generate new migration by using: `php artisan generate:migration migration_name` .  
**Seeders** are used to fill up tables with some dummy content so you can test the system without needing to go through everything. They’re located under `app/database/seeds` and managed by DatabaseSeeder.php file.


##6. System walkthrough
###Logging in
Set basic auth and enter username and password mentioned above.  
Use admin account to get access to all areas.  
To switch account, go under basic tab and enter user’s username/password.  
Then click refresh headers.  
After you see headers, you can use presets. Delete auth header with admin info then add user auth by clicking User preset.

###Routes
Routes are available as Postman collection.  
In addition to that, there is simple PHP script that generates public/documentation.html file from all available routes. You can check it out if you like, and in case you modify routes - don’t forget to run php generate-api-docs.php ;)  
Third way is to go into console and run php artisan routes which will bring up table containing every route.

###Modules (collection sections)
System is made of various modules that control different aspects of system.  
Each module has Controller and Rule, and usually (at least) one table in database.  
They are major system features.  
Most of them in postman collection have CRUD operations available.  
Column fields from table are dynamically assigned from $_POST to table-column.  
So if rule adds a column to a table, you can fill it by passing value via POST / PUT.

###Collection section: Holiday
Here you can do basic CRUD operations on holidays table.  
This table is used in conjunction with Days Without Holidays rule that counts number of days user is taking, without holidays.  
In this table, weekends should be added as well.


###Collection section: Leave type

Leave type combine rules and it’s attached to Leave.  
I suggest you check out rules first before adding leave_type.  
For example if you’d like to create type called: “Paid PTO”, you’d send:  
```
name: Paid PTO  
rules[]: 1 - Add rule by id, assuming Paid rule is id #1  
paid: 1 - This leave type is paid.  
```

###Collection section: Leaves
Leaves are user request to take time off in some period of time.  
Mostly these are modified by rules in some way.  
Each leave has leave type associated to it, which defines it’s properties and requirements.  
Eg. If you’re making PTO leave request, assuming that type has book in advance  rule activated - you won’t be able to make request eg. 1 day before leave.  
To approve leave, you you can manipulate it’s approved property (1 for yes, 0 for no).

###Collection section: Users
Users are either admins or regular users. This is determined by “admin” flag column in users table. Passwords are hashed.
Users have limited amount of days to spend on leave (added by days allowed rule).  
You can modify these numbers by passing data in POST/PUT.  
```
days_allowed_per_year: 60
days_allowed_per_month: 30
```


##7. Rules system
###Introduction
Rules system is built around the idea of pluggability and Dependency Injection as well as “programming to the interface” coding style.  
Each rule is tied to one or many leave types. This makes this relation m2m with pivot table.  
DI “exploits” that, so you can pass autoinjector parameter along with model and input reference.  
Link is added and rule is executed.  

On more basic level, rule has to implement an Interface and abstract class as well - if it uses DI.  
Interface contains “hook functions” which are called on various places (depending of method name) throughout the application.  
So rule can modify new leave request model before or after it’s saved to table, or when leave request is approved.  

Rules are stored into database and executed in order (column: order in rules table, ascending).

###Expandability / Creating new rule
If at some point we’d like to add another rule - this system design would make it very easy.  
First, we’d like to create another migration:  
```
php artisan generate:migration add_rule_name_rule
```
In that migration we’d manipulate leave / leave type schema to adjust it for new rule (eg. Paid rule added column to `leave_type`). Rules have to implement RuleInterface and extend abstract class if they plan to use Dependency Injection.  
After that, just add rule to RulesTableSeeder and create rule file under models/rules/.

###Paid
**Handle:** Paid  
**File:** paid.rule.php  
**Hook:** onCreateLeaveType

Paid rule defines leave type status. If leave type paid property is set to 1, then users taking this leave will be fully paid.

###Auto Approve
**Handle:** AutoApprove  
**File:** autoapprove.rule.php  
**Hook:** onLeaveRequest

You can add this rule to auto approve any request in this category (Leave type).  
This rule doesn’t change database schema because it manipulates with existing column in leaves table (approved).

###Require End
**Handle:** RequireEnd  
**File:** requireend.rule.php  
**Hook:** onLeaveRequest

If this rule is added on leave type, when use makes request with that leave type - he has to enter leave ending date.  
In addition, this rule makes sure that dates are in future. It is recommended that you add this rule when creating new leave type.

###Days Allowed
**Handle:** DaysAllowed  
**File:** daysallowed.rule.php  
**Hook:** onLeaveRequest

There is a limit of how many days user can take per month / per year.  
This rule checks that limit.  
It depends on Require End and Days without holidays rule to function.

###Book In Advance
**Handle:** BookInAdvance  
**File:** bookinadvance.rule.php  
**Hook:** onLeaveRequest

If you add this rule and set book_in_advance in leave type, to some number of days (lets say 5), user need to book leave with that type at least 5 days before starting date.

###Limit AFK
**Handle:** LimitAFK  
**File:** limitafk.rule.php  
**Hook:** onLeaveRequest

You can limit how many users can take leave at the same time.  
This option is attached on leave type, and not every leave type has to obey this rule.  
Eg. we can limit afks for PTO on 2 but 5 for Business trip.  
If user books PTO but 2 people already booked something in same period as he did, error will be thrown.  
But if he books Business trip, he request will be successful.  
This is because business trip is productive time, but PTO is not.  


###Days Without Holidays
**Handle:** DaysWithoutHolidays  
**File:** dayswithoutholidays.rule.php  
**Hook:** onLeaveRequest  

Holidays are predefined module in system which this rule “exploits”.  
This plugin adds new column in leaves named “days_without_holidays” .  
In that column when new leave request is added, plugin writes number of days excluding dates defined in holidays table.