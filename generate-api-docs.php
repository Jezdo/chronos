<?php
$routesTable = [];
$routesList = [];
$routes = exec("php artisan routes", $routesTable);
$counter = 0;
foreach ($routesTable as $rowIndex => $routeRow ){
    if(strpos($routeRow, "+") !== false){
        unset($routesTable[$rowIndex]);
    } else {
        $routeRow = str_replace('GET|HEAD', 'GET', $routeRow);
        $routeRowColumns = explode("|", $routeRow);
        $routesList[$counter]['domain'] = trim($routeRowColumns[1]);

        $routesList[$counter]['uri'] = trim($routeRowColumns[2]);
        $method_and_path = explode(" ", $routesList[$counter]['uri']);
        $routesList[$counter]['method'] = @trim($method_and_path[0]);
        $routesList[$counter]['path'] = @trim($method_and_path[1]);

        $routesList[$counter]['name'] = trim($routeRowColumns[3]);
        $routesList[$counter]['action'] = trim($routeRowColumns[4]);
        $routesList[$counter]['before_filters'] = trim($routeRowColumns[5]);
        $routesList[$counter]['after_filters'] = trim($routeRowColumns[6]);


        $counter++;
    }

}
array_shift ( $routesList );

$content = '';
// Code to generate docs.html
$content .=  "<ul>";
foreach($routesList as $route){
    $content .= "<li><strong>".$route['name']."</strong>";
    $content .= "<ul>";
    $content .= "<li><strong>Action:</strong> ".$route['action']."</li>";
    $content .= "<li><strong>Method:</strong> ".$route['method']."</li>";
    $content .= "<li><strong>Path:</strong> /".$route['path']."/</li>";
    $content .= "<li><strong>Before Filter:</strong> ".$route['before_filters']."</li>";
    $content .= "<li><strong>After Filters:</strong> ".$route['after_filters']."</li>";
    $content .= "<li><strong>Domain:</strong> ".$route['domain']."</li>";
    $content .= "<li><strong>URI:</strong> <em>".$route['uri']."</em></li>";

    $content .=  "</ul></li>";
}
$content .=  "</ul>";
file_put_contents("public/documentation.html", $content);