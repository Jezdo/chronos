<?php

class RuleController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /rule
	 *
	 * @return Response
	 */
	public function index()
	{
		$all = Rule::all();
        // Fetch rules from pivot table
        $counter = 0;
        $list = [];
        foreach($all as $rule){
            $list[$counter] = $rule;
            $list[$counter]['leavetypes'] = $rule->leavetypes;
            $counter++;
        }
        return $list;
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /rule
	 *
	 * @return Response
	 */
	public function store()
	{

        // Fetch $_POST content
        $input = Input::all();

        // Create new rule
        $rule = new Rule();
        // Default state of approved field is 0


        // Validate user input
        $input['active'] = (!empty($input['active']) && $input['active'] == '1') ? 1 : 0;
        $requiredFields = ['title', 'active', 'handle'];
        if($this->validateUserInput($input, $requiredFields) !== true){
            return $this->validateUserInput($input, $requiredFields);
        }

        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_' && is_string($columnValue))
                $rule->$columnName = trim($columnValue);
        }
        $rule->active = $input['active'];


        // Try to save rule, if field doesn't exist - handle error please.
        try {
            $rule->save();
        } catch(Exception $e){
            $error = 'Unknown field name. What are you doing..?'.$e->getMessage();
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }
        return $rule;
	}

	/**
	 * Display the specified resource.
	 * GET /rule/{rule}
	 *
	 * @param  int  $rule
	 * @return Response
	 */
	public function show($rule)
	{

        $rule = Rule::find($rule);

        // If user is not found, throw an error
        if(!is_object($rule)){
            $error = 'Rule not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $rule['leavetypes'] = $rule->leavetypes;

        return $rule;
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /rule/{rule}
	 *
	 * @param  int  $rule
	 * @return Response
	 */
	public function update($rule)
	{
        // Fetch $_POST content
        $input = Input::all();

        // Find rule
        $rule = Rule::find($rule);

        // If user is not found, throw an error
        if(!is_object($rule)){
            $error = 'Rule not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }


        // Validate user input
        $input['active'] = !empty($input['active']) && $input['active'] == 1 ? 1 : 0;

        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_')
                $rule->$columnName = trim($columnValue);
        }


        // Try to save rule, if field doesn't exist - handle error please.
        try {
            $rule->save();
        } catch(Exception $e){
            $error = 'Unknown field name or value. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }
        $rule->leavetypes;
        return $rule;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /rule/{rule}
	 *
	 * @param  int  $rule
	 * @return Response
	 */
	public function destroy($rule)
	{
        // Edit existing user
        $rule = Rule::find($rule);

        // If user is not found, throw an error
        if(!is_object($rule)){
            $error = 'Rule not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $rule->delete();
        return Response::json(['message'=>'Successfully deleted!','status'=>200]);
	}

}