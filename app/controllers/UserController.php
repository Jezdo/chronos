<?php

class UserController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function index()
	{
		$allUsers = User::all();
        return $allUsers;
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /user
	 *
	 * @return Response
	 */
	public function store()
	{
        // Fetch $_POST content
        $allInput = Input::all();

        // Create new user
        $user = new User();

        // Validate user input
        $requiredFields = ['username', 'password', 'email', 'admin'];
        $input['admin'] = !empty($input['admin']) && $input['admin'] == 1 ? 1 : 0;
        if($this->validateUserInput($allInput, $requiredFields) !== true){
            return $this->validateUserInput($allInput, $requiredFields);
        }

        // Check if there are users with same email or username
        if(User::whereEmail($allInput['email'])->count() > 0){
            $error = 'Email already exists!';
            $status = 500;
            return Response::json($error, $status);
        }
        if(User::whereUsername($allInput['username'])->count() > 0){
            $error = 'Username already exists!';
            $status = 500;
            return Response::json($error, $status);
        }

        // Associate field names to column names and bind values
        foreach($allInput as $columnName => $columnValue){
            // Bind column if field doesn't have underscore at the begining

            if(substr($columnName, 0, 1) != '_')
                $user->$columnName = trim($columnValue);
        }


        // Hash password
        $user->password = Hash::make($allInput['password']);
        $user->remember_token = '';


        // If need be, we could trigger hook of rules here for this module & action

        // Try to save user, if field doesn't exist - handle error please.
        try {
            $user->save();
        } catch(Exception $e){
            $error = 'Unknown field name. What are you doing..?'.$e->getMessage();
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }
        return $user;
	}

	/**
	 * Display the specified resource.
	 * GET /user/{user}
	 *
	 * @param  User  $user
	 * @return Response
	 */
	public function show($user)
	{
        $user = User::find($user);

        // If user is not found, throw an error
		if(!is_object($user)){
            $error = 'User not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }

        return $user;
	}
	/**
	 * Update the specified resource in storage.
	 * PUT /user/{user}
	 *
     * @param  User  $user
	 * @return Response
	 */
	public function update($user)
	{
        // Fetch $_POST content
        $allInput = Input::all();

        // Edit existing user
        $user = User::find($user);

        // If user is not found, throw an error
        if(!is_object($user)){
            $error = 'User not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }

        // Validate user input
        if($this->validateUserInput($allInput, []) !== true){
            return $this->validateUserInput($allInput, []);
        }

        // Associate field names to column names and bind values
        foreach($allInput as $columnName => $columnValue){
            $user->$columnName = $columnValue;
        }


        // Hash password, if changed
        if(!empty($allInput['password']))
            $user->password = Hash::make($allInput['password']);


        // If need be, we could trigger hook of rules here for this module & action

        // Try to save user, if field doesn't exist - handle error please.
        try {
            $user->save();
        } catch(Exception $e){
            $error = 'Unknown field name. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }
        return $user;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{user}
	 *
     * @param  User  $user
	 * @return Response
	 */
	public function destroy($user)
	{
        // Edit existing user
        $user = User::find($user);

        // If user is not found, throw an error
        if(!is_object($user)){
            $error = 'User not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $user->delete();
        return Response::json(['message'=>'Successfully deleted!','status'=>200]);
	}

}