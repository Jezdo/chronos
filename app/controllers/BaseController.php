<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}


    /**
     * Validate User Input
     * @param Input::all() $input - $_POST content
     * @return bool|\Illuminate\Http\JsonResponse - Either json response on error or true on success
     */
    protected function validateUserInput($input, $requiredFields){
        // Run validation check...

        foreach($requiredFields as $requiredField) {
            $value = @trim($input[$requiredField]);

            if (!array_key_exists($requiredField, $input) || $value=='' ){
                $error = 'Field '.$requiredField.' is non-existing or empty.';
                $status = 500;
                return Response::json(['error' => $error, 'status'=>$status], $status);
            }
        }

        foreach($input as $inputColumn => $inputValue){
            if(is_string($inputValue) && trim($inputValue) == ''){
                $error = 'Field '.$inputColumn.' is empty.';
                $status = 500;
                return Response::json(['error' => $error, 'status'=>$status], $status);
            }
        }

        // Validate email
        if(!empty($input['email']) && !filter_var($input['email'], FILTER_VALIDATE_EMAIL)){
            $error = 'Email is not in correct format.';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        return true;
    }

}
