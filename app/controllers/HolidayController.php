<?php

class HolidayController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /holiday
	 *
	 * @return Response
	 */
	public function index()
	{
        $all = Holiday::all();
        return $all;
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /holiday
	 *
	 * @return Response
	 */
	public function store()
	{
	    // Fetch $_POST content
        $input = Input::all();

        // Create new rule
        $holiday = new Holiday();
        // Default state of approved field is 0

        // Validate user input
        $requiredFields = ['name', 'start', 'end'];
        if($this->validateUserInput($input, $requiredFields) !== true){
            return $this->validateUserInput($input, $requiredFields);
        }

        // TODO Add validation for overlaping holidays (we can't have 2 holidays starting and ending at the same day) or 2 holidays that have same name

        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_' && is_string($columnValue))
                $holiday->$columnName = trim($columnValue);
        }


        // Try to save rule, if field doesn't exist - handle error please.
        try {
            $holiday->save();
        } catch(Exception $e){
            $error = 'Unknown field name. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        return $holiday;
	}

	/**
	 * Display the specified resource.
	 * GET /holiday/{holiday}
	 *
	 * @param  int  $holiday
	 * @return Response
	 */
	public function show($holiday)
	{
        $holiday = Holiday::find($holiday);

        // If object is not found, throw an error
        if(!is_object($holiday)){
            $error = 'Holiday not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }

        return $holiday;
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /holiday/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($holiday)
	{
        // Fetch $_POST content
        $input = Input::all();

        // Create new rule
        $holiday = Holiday::find($holiday);
        if(!is_object($holiday)){
            $error = 'Holiday not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }

        // Default state of approved field is 0

        // Validate user input
        $requiredFields = [];
        if($this->validateUserInput($input, $requiredFields) !== true){
            return $this->validateUserInput($input, $requiredFields);
        }

        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_' && is_string($columnValue))
                $holiday->$columnName = trim($columnValue);
        }


        // Try to save rule, if field doesn't exist - handle error please.
        try {
            $holiday->save();
        } catch(Exception $e){
            $error = 'Unknown field name. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        return $holiday;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /holiday/{id}
	 *
	 * @param  int  $holiday
	 * @return Response
	 */
	public function destroy($holiday)
	{
        // Edit existing user
        $holiday = Holiday::find($holiday);

        // If object is not found, throw an error
        if(!is_object($holiday)){
            $error = 'Holiday not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $holiday->delete();
        return Response::json(['message'=>'Successfully deleted!','status'=>200]);
	}

}