<?php

class LeaveController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /leave
	 *
	 * @return Response
	 */
	public function index()
	{
        $all = Leave::all();
        $list = [];
        $counter = 0;

        // Fetch rules from pivot table
        foreach($all as $leave){
            $list[$counter] = $leave;
            $leave->type->rules;
            $list[$counter]['type'] = $leave->type;
            $counter++;
        }
        return $list;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /leave
	 *
	 * @return Response
	 */
	public function store()
	{

	    // Fetch $_POST content
        $input = Input::all();

        // Create new rule
        $leave = new Leave();
        $leave->user_id = Auth::user()->id;
        // Default state of approved field is 0
        $leave->approved = 0;

        // Validate user input
        $requiredFields = ['leave_type_id', 'start', 'note'];
        if($this->validateUserInput($input, $requiredFields) !== true){
            return $this->validateUserInput($input, $requiredFields);
        }



        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_')
                $leave->$columnName = trim($columnValue);
        }




        // Try to save rule, if field doesn't exist - handle error please.
        try {

            $rulesListHolder = $leave->type->rules;
            $rulesList = [];
            $x = 0;
            foreach($rulesListHolder as $rule) {
                $rulesList[$x]["id"] = $rule->id;
                $rulesList[$x]["order"] = $rule->order;
                $x++;
            }

            // Run rules list (trigger onLeaveRequest)
            Rule::executeList($rulesList, 'onLeaveRequest', $leave, $input);


            $leave->save();

        } catch(Exception $e){
            $error = $e->getMessage();
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        return $leave;
	}

	/**
	 * Display the specified resource.
	 * GET /leave/{leave}
	 *
	 * @param  int  $leave
	 * @return Response
	 */
	public function show($leave)
	{
        $leave = Leave::find($leave);

        // If user is not found, throw an error
        if(!is_object($leave)){
            $error = 'Leave not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $leave->type->rules;

        return $leave;
	}

    /**
     * Update the specified resource in storage.
     * PUT /leave/{leave}
     *
     * @param $leave
     * @return Response
     */
	public function update($leave)
	{
        // Fetch $_POST content
        $input = Input::all();

        // Find rule
        $leave = Leave::find($leave);

        // If user is not found, throw an error
        if(!is_object($leave)){
            $error = 'Leave not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }

        // Fetch rules..
        $rulesListHolder = $leave->type->rules;

        // Lets see if approved status changed..
        $input['approved'] = isset($input['approved']) && $input['approved']=='1' ? '1' : '0';
        // If it did, trigger onLeaveApprove hooks on rules
        if($leave->approved=='0' && isset($input['approved']) && $input['approved']=='1'){

            // Run rules list (trigger onLeaveApprove)
            $rulesList = [];
            $x = 0;
            foreach($rulesListHolder as $rule) {
                $rulesList[$x]["id"] = $rule->id;
                $rulesList[$x]["order"] = $rule->order;
                $x++;
            }


            try {
                Rule::executeList($rulesList, 'onLeaveApprove', $leave, $input);
            } catch(Exception $e){
                $error = $e->getMessage();
                $status = 404;
                return Response::json(['error'=>$error,'status'=>$status], $status);
            }
        }

        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_')
                $leave->$columnName = trim($columnValue);
        }


        // Try to save rule, if field doesn't exist - handle error please.
        try {
            $leave->save();
        } catch(Exception $e){
            $error = 'Unknown field name or value. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        return $leave;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /leave/{leave}
	 *
	 * @param  int  $leave
	 * @return Response
	 */
	public function destroy($leave)
	{
        // Edit existing user
        $leave = Leave::find($leave);

        // If object is not found, throw an error
        if(!is_object($leave)){
            $error = 'Leave not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $leave->delete();
        return Response::json(['message'=>'Successfully deleted!','status'=>200]);
	}

}