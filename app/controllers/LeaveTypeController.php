<?php

class LeaveTypeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /leavetype
	 *
	 * @return Response
	 */
	public function index()
	{
        $all = LeaveType::all();
        $list = [];
        $counter = 0;

        // Fetch rules from pivot table
        foreach($all as $leavetype){
            $list[$counter] = $leavetype;
            $list[$counter]['rules'] = $leavetype->rules;
            $counter++;
        }
        return $list;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /leavetype
	 *
	 * @return Response
	 */
	public function store()
	{

	    // Fetch $_POST content
        $input = Input::all();

        // Create new rule
        $leavetype = new LeaveType();
        // Default state of approved field is 0

        // Validate user input
        $requiredFields = ['name'];
        if($this->validateUserInput($input, $requiredFields) !== true){
            return $this->validateUserInput($input, $requiredFields);
        }



        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){

            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_' && is_string($columnValue))
                $leavetype->$columnName = trim($columnValue);
        }


        // Try to save rule, if field doesn't exist - handle error please.
        try {


            $rules = $input['rules'];
            $rulesList = [];
            $x = 0;
            foreach($rules as $rule) {
                $rule = Rule::find($rule);
                if(is_object($rule)) {
                    $rulesList[$x]["id"] = $rule->id;
                    $rulesList[$x]["order"] = $rule->order;
                    $x++;
                }
            }

            // Run rules list (trigger onLeaveRequest)
            Rule::executeList($rulesList, 'onCreateLeaveType', $leavetype, $input);

            $leavetype->save();
            // Add these rules to database, in pivot table
            $leavetype->rules()->sync($rules);

            // Run rules list (trigger afterCreateLeaveType)
            Rule::executeList($rulesList, 'afterCreateLeaveType', $leavetype, $input);

        } catch(Exception $e){
            $error = $e->getMessage();
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }

        // Fetch rules and display array
        $leavetype = LeaveType::find($leavetype->id);
        $leavetype['rules'] = $leavetype->rules;

        return $leavetype;
	}

	/**
	 * Display the specified resource.
	 * GET /leavetype/{leavetype}
	 *
	 * @param  int  $leavetype
	 * @return Response
	 */
	public function show($leavetype)
	{

        $leavetype = LeaveType::find($leavetype);
        // If object is not found, throw an error
        if(!is_object($leavetype)){
            $error = 'Leave type not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        // Fetch rules from many to many
        $leavetype['rules'] = $leavetype->rules;

        return $leavetype;
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /leavetype/{leavetype}
	 *
	 * @param  int  $leavetype
	 * @return Response
	 */
	public function update($leavetype)
	{

        // Fetch $_POST content
        $input = Input::all();

        // Find rule
        $leavetype = LeaveType::find($leavetype);

        // If user is not found, throw an error
        if(!is_object($leavetype)){
            $error = 'Leave type not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }


        // Associate field names to column names and bind values
        foreach($input as $columnName => $columnValue){
            // Bind column if field doesn't have underscore at the begining
            if(substr($columnName, 0, 1) != '_' && is_string($columnValue))
                $leavetype->$columnName = trim($columnValue);
        }


        // Try to save object, if field doesn't exist - handle error.
        try {
            // Fetch rules
            $rules = $input['rules'];
            $rulesList = [];
            foreach($rules as $ruleid){
                $rulesList[] = $ruleid;
            }


            // Add these rules to database, in pivot table
            $leavetype->rules()->sync($rules);

            $leavetype->save();
        } catch(Exception $e){
            $error = 'Unknown field name or value. What are you doing..?';
            $status = 500;
            return Response::json(['error' => $error, 'status'=>$status], $status);
        }
        $leavetype->rules;

        return $leavetype;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /leavetype/{leavetype}
	 *
	 * @param  int  $leavetype
	 * @return Response
	 */
	public function destroy($leavetype)
	{
        // Edit existing user
        $leavetype = LeaveType::find($leavetype);

        // If object is not found, throw an error
        if(!is_object($leavetype)){
            $error = 'Leavetype not found';
            $status = 404;
            return Response::json(['error'=>$error,'status'=>$status], $status);
        }
        $leavetype->delete();
        return Response::json(['message'=>'Successfully deleted!','status'=>200]);
	}

}