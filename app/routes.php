<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/********[ 1. Users Routes ]********/

# Handle fetching all users info
Route::get("user", [
    "as"   => "user/index",
    "uses" => "UserController@index",
    "before" => "auth.basic", # Logged in only | Admin only
    "after" => "",
]);

# Handle creating new user
Route::post("user", [
    "as"   => "user/store",
    "uses" => "UserController@store",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle getting information about specific user
Route::get("user/{user}", [
    "as"   => "user/show",
    "uses" => "UserController@show",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle updating of specific user
Route::put("user/{user}", [
    "as"   => "user/update",
    "uses" => "UserController@update",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle user deletion
Route::delete("user/{user}", [
    "as"   => "user/destroy",
    "uses" => "UserController@destroy",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);


/********[ 2. Leaves Routes ]********/

# Handle fetching all leaves
Route::get("leave", [
    "as"   => "leave/index",
    "uses" => "LeaveController@index",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle creating new leave
Route::post("leave", [
    "as"   => "leave/store",
    "uses" => "LeaveController@store",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle getting information about specific leave
Route::get("leave/{leave}", [
    "as"   => "leave/show",
    "uses" => "LeaveController@show",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle updating of specific leave
Route::put("leave/{leave}", [
    "as"   => "leave/update",
    "uses" => "LeaveController@update",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle leave deletion
Route::delete("leave/{leave}", [
    "as"   => "leave/destroy",
    "uses" => "LeaveController@destroy",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);


/********[ 3. Holiday Routes ]********/

# Handle fetching all holidays
Route::get("holiday", [
    "as"   => "holiday/index",
    "uses" => "HolidayController@index",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle creating new holiday
Route::post("holiday", [
    "as"   => "holiday/store",
    "uses" => "HolidayController@store",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle getting information about specific holiday
Route::get("holiday/{holiday}", [
    "as"   => "holiday/show",
    "uses" => "HolidayController@show",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle updating of specific holiday
Route::put("holiday/{holiday}", [
    "as"   => "holiday/update",
    "uses" => "HolidayController@update",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle holiday deletion
Route::delete("holiday/{holiday}", [
    "as"   => "holiday/destroy",
    "uses" => "HolidayController@destroy",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);


/********[ 4. Leave types Routes ]********/

# Handle fetching all leaves
Route::get("leavetype", [
    "as"   => "leavetype/index",
    "uses" => "LeaveTypeController@index",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle creating new leave
Route::post("leavetype", [
    "as"   => "leavetype/store",
    "uses" => "LeaveTypeController@store",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle getting information about specific leave
Route::get("leavetype/{leavetype}", [
    "as"   => "leavetype/show",
    "uses" => "LeaveTypeController@show",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle updating of specific leave
Route::put("leavetype/{leavetype}", [
    "as"   => "leavetype/update",
    "uses" => "LeaveTypeController@update",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle leave deletion
Route::delete("leavetype/{leavetype}", [
    "as"   => "leavetype/destroy",
    "uses" => "LeaveTypeController@destroy",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);


/********[ 5. Rule Routes ]********/
# Handle fetching all leaves
Route::get("rule", [
    "as"   => "rule/index",
    "uses" => "RuleController@index",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle creating new leave
Route::post("rule", [
    "as"   => "rule/store",
    "uses" => "RuleController@store",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle getting information about specific leave
Route::get("rule/{rule}", [
    "as"   => "rule/show",
    "uses" => "RuleController@show",
    "before" => "auth.basic", # Logged in only
    "after" => "",
]);

# Handle updating of specific leave
Route::put("rule/{rule}", [
    "as"   => "rule/update",
    "uses" => "RuleController@update",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);

# Handle leave deletion
Route::delete("rule/{rule}", [
    "as"   => "rule/destroy",
    "uses" => "RuleController@destroy",
    "before" => "auth.basic|auth.admin", # Logged in only | Admin only
    "after" => "",
]);