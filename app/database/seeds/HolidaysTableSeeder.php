<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class HolidaysTableSeeder extends Seeder {

	public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            $start = date("Y-m-d 00:00:00", $faker->dateTimeBetween('-1 month', 'now')->gettimestamp());
            $end = date("Y-m-d 00:00:00", $faker->dateTimeBetween($start, $start . '+'.rand(1,10).' days')->gettimestamp());
            while($start == $end) {
                $end = date("Y-m-d 00:00:00", $faker->dateTimeBetween($start, $start . '+' . rand(1, 10) . ' days')->gettimestamp());
            }
            $holiday = new Holiday();
            $holiday->name = $faker->text(15);
            $holiday->start = $start;
            $holiday->end = $end;
            $holiday->save();

        }
    }

}