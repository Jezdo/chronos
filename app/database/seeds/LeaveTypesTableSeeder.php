<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LeaveTypesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
            $leavetype = new LeaveType();
            $leavetype->name = $faker->text(30);
            $leavetype->paid = rand(0,1); // Boolean, if this vacation is paid
            $leavetype->book_in_advance = rand(5,10); // Number of days that we need to book in advance
            $leavetype->limit_afk = rand(3,5); // Number of users to limit with same starting and ending date
            $leavetype->save();
		}
	}

}