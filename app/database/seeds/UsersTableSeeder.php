<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
        // Instantiate faker so we can get some realistic data...
		$faker = Faker::create();

        $user = new User();
        $user->username = 'admin';
        $user->password = Hash::make('password');
        $user->email = 'admin@server';
        $user->admin = 1;
        $user->days_allowed_per_year = 200;
        $user->days_allowed_per_month = 30;
        $user->save();

        $user = new User();
        $user->username = 'user';
        $user->password = Hash::make('password');
        $user->email = 'user@server';
        $user->admin = 0;
        $user->save();

		foreach(range(1, 10) as $index)
		{
            $user = new User();
            $user->username = $faker->userName;
            $user->password = Hash::make('password');
            $user->email = $faker->email;
            $user->admin = 1;
            $user->save();
		}
	}

}