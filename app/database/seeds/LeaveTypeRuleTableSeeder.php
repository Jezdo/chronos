<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LeaveTypeRuleTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 50) as $index)
		{
			LeaveTypeRule::create([
                'leave_type_id' => rand(1,10),
                'rule_id' => rand(1,7),
            ]);
		}
	}

}