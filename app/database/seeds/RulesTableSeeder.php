<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RulesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        $availableRuleFiles = ['Paid', 'AutoApprove', 'RequireEnd', 'LimitAFK', 'DaysAllowed', 'DaysWithoutHolidays', 'BookInAdvance'];

		foreach(range(1, count($availableRuleFiles)) as $index)
		{
			$rule = new Rule();
            $rule->title = $availableRuleFiles[$index-1].' rule';
            $rule->active = 1;
            $rule->handle = $availableRuleFiles[$index-1];
            $rule->order = $index;
            $rule->save();
		}
	}

}