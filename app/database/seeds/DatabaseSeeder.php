<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

//        // Seed users table
		 $this->call('UsersTableSeeder');
//        // Seed holidays table
		 $this->call('HolidaysTableSeeder');
//        // Seed Leave types
		 $this->call('LeaveTypesTableSeeder');
//        // Seed rules table
		 $this->call('RulesTableSeeder');
        // Seed pivot table (Leave_type_rule)
         $this->call('LeaveTypeRuleTableSeeder');
        // Seed leaves table
         $this->call('LeavesTableSeeder');
	}

}
