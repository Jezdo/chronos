<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LeavesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        $limit = 50;
        echo 'Seeding LeavesTableSeeder...'."\n";
		foreach(range(1, $limit) as $index)
		{

            $start = date("Y-m-d 00:00:00", $faker->dateTimeBetween('-1 month', 'now')->gettimestamp() );
            $end = date("Y-m-d 00:00:00", $faker->dateTimeBetween($start, '+10 days')->gettimestamp() );
            if($start == $end){
                $end = date("Y-m-d 00:00:00", $faker->dateTimeBetween($start, '+10 days')->gettimestamp() );
            }
			$leave = new Leave();
            $leave->user_id = rand(2,10);
            $leave->leave_type_id = rand(1,10);
            $leave->start = $start;
            $leave->end = $end;
            $leave->note = $faker->sentence();
            $leave->approved = rand(0,1);
            $leave->save();
            echo ($index % ($limit/10) == 0) ? "[ $index / $limit ]\n" : '';
		}
        echo "\n";
	}

}