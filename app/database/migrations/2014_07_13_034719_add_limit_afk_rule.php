<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLimitAfkRule extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leave_types', function(Blueprint $table)
		{
            // Limit to 5 users
			$table->integer('limit_afk')->default(5);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leave_types', function(Blueprint $table)
		{
			$table->dropColumn('limit_afk');
		});
	}

}
