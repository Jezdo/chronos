<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leave_types', function(Blueprint $table)
		{
            // Handle engine and primary key parameters
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');

            // Add timestamp
            $table->timestamps();


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leave_types', function(Blueprint $table)
		{
			Schema::dropIfExists('leave_types');
		});
	}

}
