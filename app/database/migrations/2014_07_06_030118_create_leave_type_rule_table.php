<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeaveTypeRuleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leave_type_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('leave_type_id')->unsigned()->index();
			$table->foreign('leave_type_id')->references('id')->on('leave_types')->onDelete('cascade');
			$table->integer('rule_id')->unsigned()->index();
			$table->foreign('rule_id')->references('id')->on('rules')->onDelete('cascade');


            // Add timestamp
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leave_type_rule');
	}

}
