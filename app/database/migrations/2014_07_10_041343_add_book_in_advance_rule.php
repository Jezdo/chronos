<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBookInAdvanceRule extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leave_types', function(Blueprint $table)
		{
			$table->integer('book_in_advance')->default(5);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leave_types', function(Blueprint $table)
		{
            $table->dropColumn('book_in_advance');
		});
	}

}
