<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rules', function(Blueprint $table)
		{
           // Handle engine and primary key parameters
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title'); // Human-readable name of this rule
            $table->boolean('active'); // Enable/disable rules globally.
            $table->string('handle'); // Rule class name
            $table->integer('order'); // Order of execution (in range 1 to 10)


            // Add timestamp
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rules', function(Blueprint $table)
		{
            Schema::dropIfExists('rules');
		});
	}

}
