<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leaves', function(Blueprint $table)
		{
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            // Handle engine and primary key parameters
            $table->engine = 'InnoDB';
            $table->increments('id');

            // Add user id FK
            $table->integer('user_id')->unsigned()->index();
            $table  ->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            // Add Leave type id FK
            $table->integer('leave_type_id')->unsigned()->index();
            $table  ->foreign('leave_type_id')
                    ->references('id')->on('leave_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->dateTime('start');
            $table->dateTime('end');

            $table->text('note');
            $table->boolean('approved');

            // And timestamp
            $table->timestamps();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('leaves');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}
