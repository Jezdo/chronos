<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
            // Handle engine and primary key parameters
            $table->engine = 'InnoDB';
            $table->increments('id');

            // Add fields
            $table->string('username', 30);
            $table->string('password', 64);
            $table->string('email', 64);

            // Is this user administrator?
            $table->boolean('admin');

            // Add remember me token
            $table->rememberToken();

            // And timestamp
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users');
	}

}
