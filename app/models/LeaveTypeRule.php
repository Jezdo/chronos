<?php

/**
 * Class LeaveTypeRule
 * This model is just helper to associate leave types with rules.
 * 
 * It manages with pivot table needed for m2m relation.
 *
 * @author Jezdimir Loncar <jezdimir.loncar@gmail.com>
 * @property integer $id
 * @property integer $leave_type_id
 * @property integer $rule_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\LeaveTypeRule whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveTypeRule whereLeaveTypeId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveTypeRule whereRuleId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveTypeRule whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveTypeRule whereUpdatedAt($value) 
 */
class LeaveTypeRule extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'leave_type_rule';

}