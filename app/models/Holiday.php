<?php

/**
 * Holiday
 *
 * @property integer $id
 * @property string $name
 * @property string $start
 * @property string $end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereStart($value) 
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereEnd($value) 
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Holiday whereUpdatedAt($value) 
 */
class Holiday extends \Eloquent {
	protected $fillable = [];
}