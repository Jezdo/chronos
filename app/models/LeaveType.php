<?php

/**
 * LeaveType
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\LeaveType whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveType whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveType whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\LeaveType whereUpdatedAt($value) 
 */
class LeaveType extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leave_types';

    /**
     * Define m2m relationship with rules
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rules(){
        // Define extra columns in pivot table that rules will be using
        return $this->belongsToMany('Rule')->withTimestamps()->groupBy('rule_id')->orderBy('order', 'asc');
    }
    /**
     * Define m2m relationship with leaves
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaves(){
        return $this->belongsTo('Leaves')->groupBy('id');
    }
}