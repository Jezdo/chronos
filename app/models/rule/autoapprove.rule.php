<?php
class AutoApprove implements RuleInterface {

    /**
     * This class hooks
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        /**
         * Auto approve model
         */
        $model->approved = 1;
    }

    /**
     * Implemented, but not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
        echo "Triggering onLeaveApprove from AutoApprove..";
    }

    /**
     * Implemented, but not in use for this rule.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['rules']['autoapprove']['option1'] - This allows user to pass option to rule
     * @return void
     * @deprecated
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
        var_dump("Triggering onCreateLeaveType from AutoApprove..");
//        var_dump("\nHello =: ".$input['rules']['autoapprove']['hello']);
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
        var_dump("Triggering afterCreateLeaveType from AutoApprove..");
    }
}