<?php
use Carbon\Carbon;
class LimitAFK extends RuleAbstract implements RuleInterface {
    /**
     * This class hooks
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @throws Exception
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        $leaveType = LeaveType::find($model->leave_type_id);

        if(!is_object($leaveType)){
            $error = 'Unable to find Leave Type #'.$model->leave_type_id;
            throw new Exception($error);
        }

        $leaveTypeMaximumAFKAllowed = $leaveType->limit_afk; // Maximum number of users allowed to leave at the same time

        /**
         * Alternately, we could have add column to rules table and set Limit AFK globally, but this is a bit more granular.
         * Eg. we can limit afks for PTO on 2 but 5 for Business trip.
         * If user books PTO but 2 people already booked something in same period as he did, error will be thrown.
         * But if he books Business trip, he request will be successful.
         */
        if($leaveTypeMaximumAFKAllowed <= 0){
            $error = 'Maximum number of people allowed to go away should be greater than zero. You can always remove rule from type ;)';
            throw new Exception($error);
        }

        /**
         * Get all bookings for this date
         */
//        $bookingsCounts = DB::statement('select start, end, COUNT(1) from leaves group by start, end having count(*) > 1');
        // Get bookings for all dates
//        $bookingsCount = DB::table('leaves')
//                          ->select(DB::raw('start, end, COUNT(1)'))
//                          ->groupBy('start', 'end')
//                          ->havingRaw('count(*) > 1')
//                          ->get();
        // Get bookings for this
        /**
         * This is very strict query that finds only completely overlapping periods
         */
        $bookingsCount = DB::table('leaves')
                          ->select(DB::raw('start, end, COUNT(1) as leavesBooked'))
                          ->groupBy('start', 'end')
                          ->havingRaw('COUNT(*) > 1 AND start = "'.$model->start.'" AND end="'.$model->end.'"')
                          ->first();

        // If there are no bookings with this date
        if($bookingsCount == null){
                return; // Just skip this
        }
        // If there are, check the limit
        if ( ($bookingsCount->leavesBooked+1) > $leaveTypeMaximumAFKAllowed) {
            $error = 'Too many people booked this time period already! '.$bookingsCount->leavesBooked." of ".$leaveTypeMaximumAFKAllowed;
            throw new Exception($error);
        }



    }

    /**
     * Implemented, but not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
//        echo "Triggering onLeaveApprove from AutoApprove..";
    }

    /**
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['book_in_advance']
     * @return void
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
        /**
         * Set how many days we want this leave type to be booked in advance..
         */
        $model->book_in_advance = isset($input['book_in_advance']) ? $input['book_in_advance'] : 0;
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering afterCreateLeaveType from AutoApprove..");
    }
}