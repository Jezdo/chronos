<?php
use Carbon\Carbon as Carbon;

/**
 * Class DaysAllowed
 * @see RuleAbstract - Depends on abstract rule class for DI
 * @see RequireEnd - Depends on RequireEnd rule to be enabled.
 */
class DaysAllowed extends RuleAbstract implements RuleInterface {

    /**
     * This class hooks when leave request is made
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @throws Exception
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        // Try to inject rule
        try{
            RuleAbstract::requireRule('RequireEnd',
                $model->leave_type_id,
                true,
                'onLeaveRequest',
                $model,
                $input);
        } catch(Exception $e){
            throw new Exception('[Inject][RequireEnd]: '.$e->getMessage());
        }
        // TODO Implement at some point - exclude holidays from previous vacations and make DaysAllowed work without counting holidays
//        try{
//            RuleAbstract::requireRule('DaysWithoutHolidays',
//                $model->leave_type_id,
//                true,
//                'onLeaveRequest',
//                $model,
//                $input);
//        } catch(Exception $e){
//            throw new Exception('[Inject][DaysWithoutHolidays]: '.$e->getMessage());
//        }
        /**
         * Count number of days this user had in this year / month, add delta ($model->end-$model->start)
         * and compare that with Auth::user()->days_allowed_per_year and Auth::user()->days_allowed_per_month
         */
        $daysAllowedPerYear = Auth::user()->days_allowed_per_year;
        $daysAllowedPerMonth = Auth::user()->days_allowed_per_month;

        /**
         * Start counting number of days user left
         */
        // Select all user leaves
        $userLeaves = Leave::whereUserId(Auth::user()->id)->orderBy('start', 'asc')->get();

        // Get start and end date for this request
        $start = new Carbon($model->start);
        $end = new Carbon($model->end);

        // If days difference between start and end is greater then yearly limit - throw an error
        // We can only allow booking of normal leave period. We can't book user leave on 2 years...
        if($model->days_without_holidays > $daysAllowedPerYear){
            $error = 'Difference between start and end is greater then yearly limit of '.$daysAllowedPerYear.' days.';
            throw new Exception($error);
        }

        // Start counting days this user was absent in this month
        // Also start counting number of days user was absent in this year
        $daysUserLeftThisMonth = 0;
        $daysUserLeftThisYear = 0;


        $now = new Carbon("now");

        if($start->month+1 >= 13)
            $beggingOfNextMonth = new Carbon(($start->year+1)."-01-01 00:00:00");
        else
            $beggingOfNextMonth = new Carbon($start->year."-".($start->month+1)."-01 00:00:00");

        $beggingOfNextYear  = new Carbon(($start->year+1)."-01-01 00:00:00");
        $thisYear = $now->year;
        $thisMonth = $now->month;

        $lastUserLeaveEndTime = null;
        // If user already had booked leave
        if(count($userLeaves) != 0) {
            // Loop through leaves
            // And store last one for check
            foreach ($userLeaves as $userLeave) {
                // And instantiate datetime manager
                $carbonUserStart = new Carbon($userLeave->start);
                $carbonUserEnd = new Carbon($userLeave->end);


                // TODO Only use daysWithoutHolidays here...
                
                // And calculate absent days for this month and year only
                if ($carbonUserStart->month == $thisMonth && $carbonUserStart->year == $thisYear) {
                    // If there are some days selected in next month, don't care about them
                    $daysUserLeftThisMonth += ($carbonUserEnd->month > $carbonUserStart->month) ? $carbonUserStart->diffInDays($beggingOfNextMonth) : $carbonUserStart->diffInDays($carbonUserEnd);
                }
                // And this year
                if ($carbonUserStart->year == $thisYear) {
                    // Dont care about days that are queued for next year...
                    $daysUserLeftThisYear += ($carbonUserEnd->year > $carbonUserStart->year) ? $carbonUserStart->diffInDays($beggingOfNextYear) : $carbonUserStart->diffInDays($carbonUserEnd);
                }
                // last
                $lastUserLeaveEndTime = $carbonUserEnd;
            }
        }
        $daysQueringToBookThisMonth = (($end->month > $start->month)) ? $start->diffInDays($beggingOfNextMonth) : $start->diffInDays($end);
        $daysQueringToBookThisYear = ($end->year > $start->year) ? $start->diffInDays($beggingOfNextYear) : $start->diffInDays($end);



//      Some very useful debugging info :)
//        echo "Days already left this month: ".$daysUserLeftThisMonth."\nThis year: ".$daysUserLeftThisYear."\nAnd wants to leave for: ".$daysQueringToBookThisMonth." days this month, and ".$daysQueringToBookThisYear." days this year.";
//        echo "\n";
//        echo "That would total: \n";
//        echo ($daysQueringToBookThisYear+$daysUserLeftThisYear)." days this year of ".$daysAllowedPerYear." days allowed.\n";
//        echo ($daysQueringToBookThisMonth+$daysUserLeftThisMonth)." days this month of ".$daysAllowedPerMonth." days allowed.\n";
//        dd();

        // Now check if user is allowed to book his vacation
        if(($daysQueringToBookThisYear+$daysUserLeftThisYear) > $daysAllowedPerYear){
            $error = '[DaysAllowed]: Aww, you can\'t book that many days in this year...';
            throw new Exception($error);
        }
        if(($daysQueringToBookThisMonth+$daysUserLeftThisMonth) > $daysAllowedPerMonth){
            $error = '[DaysAllowed]: Aww, you can\'t book that many days in this month...';
            throw new Exception($error);
        }

        // Check if user tries to be on two vacations at once!
        // We can't allow start time of this vacation to be before the date he last time returned from vacation
        // Eg. You can't end your leave on 18th and go back to leave on 18th or 17th.
        if($lastUserLeaveEndTime != null && $lastUserLeaveEndTime->diffInDays($start, false) <= 0){
            $error = '[DaysAllowed]: You can\'t be on two vacations at the same time...';
            throw new Exception($error);
        }





    }

    /**
     * Implemented, but not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
        echo "Triggering onLeaveApprove from AutoApprove..";
    }

    /**
     * Implemented, but not in use for this rule.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['rules']['autoapprove']['option1'] - This allows user to pass option to rule
     * @return void
     * @deprecated
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
        var_dump("Triggering onCreateLeaveType from AutoApprove..");
//        var_dump("\nHello =: ".$input['rules']['autoapprove']['hello']);
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
        var_dump("Triggering afterCreateLeaveType from AutoApprove..");
    }
}