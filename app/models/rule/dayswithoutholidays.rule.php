<?php
use Carbon\Carbon;
class DaysWithoutHolidays implements RuleInterface {

    /**
     * This class hooks
     * @param Leave $model -  Leave object this funciton can modify
     * @param array $input - $_POST array
     * @throws Exception
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        $start = new Carbon($model->start);
        $end = new Carbon($model->end);
        $daysWithHolidays = $start->diffInDays($end);

//        $getHolidays = 'SELECT start, end FROM holidays
//                        WHERE start BETWEEN "2014-06-01 00:00:00" AND "2014-07-30 00:00:00"';

//        $getHolidays = DB::select('SELECT start, end FROM holidays
//                        WHERE start BETWEEN "2014-06-01 00:00:00" AND "2014-07-30 00:00:00"');
        $getHolidays = DB::table('holidays')
                            ->select(array('start','end'))
                            ->whereBetween('start', array($start, $end))
                            ->get();

        // Count how many holidays days overlap with our leave start and end
        $holidaysCounter = 0;
        foreach($getHolidays as $holiday){
            $holidayStart = new Carbon($holiday->start);
            $holidayEnd = new Carbon($holiday->end);

            if($holidayEnd->gt($end)) // If holiday ends after our vacation, calculate diff untill end of our vacation
                $holidaysCounter += $holidayStart->diffInDays($end);
            else
                $holidaysCounter += $holidayStart->diffInDays($holidayEnd);
        }

        $daysWithoutHolidays = abs($daysWithHolidays - $holidaysCounter);

        // Useful debugging information
//        echo "Days with holidays: ".$daysWithHolidays."\n";
//        echo "Holidays (non-working days) between start and end: ".$holidaysCounter."\n";
//        echo "Days without holidays: ".$daysWithoutHolidays."\n";

        $model->days_without_holidays = $daysWithoutHolidays;

    }

    /**
     * Not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
//        var_dump("Triggering onLeaveApprove from RequireEnd..");
    }

    /**
     * Implemented, but not in use for this rule.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['rules']['autoapprove']['option1'] - This allows user to pass option to rule
     * @return void
     * @deprecated
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering onCreateLeaveType from RequireEnd..");
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering afterCreateLeaveType from RequireEnd..");
    }
}