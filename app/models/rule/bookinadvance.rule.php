<?php
use Carbon\Carbon;
class BookInAdvance implements RuleInterface {

    /**
     * This class hooks
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @throws Exception
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        $leaveType = LeaveType::find($model->leave_type_id);

        if(!is_object($leaveType)){
            $error = 'Unable to find Leave Type #'.$model->leave_type_id;
            throw new Exception($error);
        }

        $leaveTypeDaysRequirement = $leaveType->book_in_advance;
        $carbonStart = new Carbon($model->start);

        // If book days in advance rule is enabled (greater than 0) and days difference between start date and today is less then required
        if( $leaveTypeDaysRequirement > 0 &&
            $carbonStart->diffInDays(new Carbon('now')) < $leaveTypeDaysRequirement) {
            $error = 'You need to book leave at least '.$leaveTypeDaysRequirement.' days before leaving. You booked: '.$carbonStart->diffInDays(new Carbon('now'));
            throw new Exception($error);
        }

    }

    /**
     * Implemented, but not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
//        echo "Triggering onLeaveApprove from AutoApprove..";
    }

    /**
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['book_in_advance']
     * @return void
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
        /**
         * Set how many days we want this leave type to be booked in advance..
         */
        $model->book_in_advance = isset($input['book_in_advance']) ? $input['book_in_advance'] : 0;
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering afterCreateLeaveType from AutoApprove..");
    }
}