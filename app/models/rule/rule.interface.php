<?php
interface RuleInterface {
    /**
     * @note
     * Input::all()['RuleHook']['param1'] = 'something'
     * Rules might require some additional parameters to be passed via $input
     */

    /**
     * This method is used to hook when new leave request is created.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input);


    /**
     * This method is used to hook when leave request is approved.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     */
    public static function onLeaveApprove(&$model, &$input);


    /**
     * This method is used to hook when new leave request is created.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     */
    public static function onCreateLeaveType(&$model, &$input);

}