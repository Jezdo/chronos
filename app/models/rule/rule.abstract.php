<?php
/**
 * Dependency Injection manager
 * It is recommended that rules extend this class if they're going to depend on some other rules.
 * This class should never be instantiated, only inherited by plugins.
 */
abstract class RuleAbstract {

    /**
     * @param Object|string|int $ruleHandle - Either handle, id or rule itself.
     * @param $leaveTypeId - Id of leave type.
     * @param bool $autoAssociate - If this is turned on (true) if leaveType and rule are not associated, they will be linked automatically.
     * @param string $method - Method to pass to depending rule
     * @param null $model - Model passed to parent rule to be passed to depending child
     * @param null $input - $_POST passed to parent rule
     * @throws Exception
     * @return boolean - True if rule is present or if wasn't but was autoincluded, exception if rule could not be instantiated or no links are present
     */
    public static function requireRule($ruleHandle,
                                       $leaveTypeId,
                                       $autoAssociate = false,
                                       $method = "",
                                       &$model = null,
                                       &$input = null) {

        /**
         * We can require some rule to be associated
         */
        // If object is passed as ruleHandle...
        if(is_object($ruleHandle)){
            $rule = $ruleHandle;
        }
        else if(is_string($ruleHandle)) {
            $rule = Rule::whereHandle($ruleHandle)->first();
            if (!is_object($rule)) {
                throw new Exception('Rule handle ' . $ruleHandle . ' is invalid.');
            }
        }
        else {
            $rule = Rule::find($ruleHandle);
            if (!is_object($rule)) {
                throw new Exception('Rule handle ' . $ruleHandle . ' is invalid.');
            }
        }

        // Check if this rule and leave type are linked
        $ruleAndTypeLinksCount = LeaveTypeRule::where('rule_id','=',$rule->id)->where('leave_type_id','=',$leaveTypeId)->count();
        // They are not associated
        if($ruleAndTypeLinksCount <= 0){
            // If autoAssociate is enabled, insert link
            if($autoAssociate){
                $newLink = new LeaveTypeRule();
                $newLink->leave_type_id = $leaveTypeId;
                $newLink->rule_id = $rule->id;
                try {
                    // Save into database
                    $newLink->save();
                    // And call the rule
                    $ruleHandle::$method($model, $input);

                } catch (Exception $e){
                    throw new Exception('Auto association failed. '.$e->getMessage());
                }
                return true;
            } else {
                throw new Exception('No links between rule '.$rule->handle.' (#'.$rule->id.') and LeaveType #'.$leaveTypeId);
            }
        }

        // Pair is already linked, return true.
        return true;

    }

    /**
     * Execute rule by handle and method
     * @param string $handle Rule handle (class) to be executed
     * @param string $method Method which we're executing (onLeaveRequest | onLeaveApprove | onCreateLeaveType)
     * @param Leave $model Leave request this rule can modify
     * @param array $input $_POST array containing user submitted information
     * @throws Exception When unable to find rule file.
     * @throws Exception When unable to find rule class.
     * @throws Exception When class doesn't implement appropriate interface.
     */
    public static function execute($handle, $method, &$model, &$input){
        // Generate file name and path
        $filename = strtolower($handle).".rule.php";
        $filepath = realpath(__DIR__."/".$filename);
        // Throw exception if file doesn't exist
        if(!$filepath){
            throw new Exception('Unable to find rule file: '.$filename);
        }

        // If it does, include it
        include_once $filepath;

        // Then check for class
        if(!class_exists($handle)){
            throw new Exception('Unable to find rule class: '.$handle);
        }

        // And then for interface
        $interfaces = class_implements($handle);
        if(empty($interfaces)){
            throw new Exception('Your class doesn\'t implement interface "Rule"');
        }

        // If all conditions are satisfied, run method
        $handle::$method($model, $input);


    }

}