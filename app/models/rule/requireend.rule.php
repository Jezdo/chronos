<?php
use Carbon\Carbon;
class RequireEnd implements RuleInterface {

    /**
     * This class hooks
     * @param Leave $model -  Leave object this funciton can modify
     * @param array $input - $_POST array
     * @throws Exception
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
        /**
         * If ending date is missing, throw an exception.
         */
        if(!isset($input['end']) || trim($input['end']) == ''){
            $error = 'You need to enter ending date.';
            throw new Exception($error);
        }
        /**
         * If ending date is in past, throw an exception.
         */
        $carbonInputStart = new Carbon($input['start']);
        $carbonInputEnd = new Carbon($input['end']);
        if($carbonInputEnd->isPast() || $carbonInputStart->isPast()){
            $error = 'Starting and ending date cannot be in past.';
            throw new Exception($error);
        }

        /**
         * Check for ending date validity (it can't be before starting date)
         */
        if($carbonInputEnd->lte($carbonInputStart)){
            $error = 'Ending date must be after starting date.';
            throw new Exception($error);
        }

        if($carbonInputEnd->diffInMinutes($carbonInputStart, true) <= 0){
            $error = 'Ending date must be after starting date.';
            throw new Exception($error);
        }

    }

    /**
     * Not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
//        var_dump("Triggering onLeaveApprove from RequireEnd..");
    }

    /**
     * Implemented, but not in use for this rule.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['rules']['autoapprove']['option1'] - This allows user to pass option to rule
     * @return void
     * @deprecated
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering onCreateLeaveType from RequireEnd..");
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
//        var_dump("Triggering afterCreateLeaveType from RequireEnd..");
    }
}