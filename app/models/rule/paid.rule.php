<?php
class Paid implements RuleInterface {

    /**
     * This class hooks
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     */
    public static function onLeaveRequest(&$model, &$input)
    {
    }

    /**
     * Implemented, but not in use for this rule.
     * Does not trigger of itself (after onLeaveRequest method of this class approved leave).
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @return void
     * @deprecated
     */
    public static function onLeaveApprove(&$model, &$input)
    {
//        echo "Triggering onLeaveApprove from AutoApprove..";
    }

    /**
     * Implemented, but not in use for this rule.
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     * @see $input['paid'] - This allows user to pass option to this rule rule (and set if this type is paid)
     * @return void
     */
    public static function onCreateLeaveType(&$model, &$input)
    {
        /**
         * Set paid status
         */
        $model->paid = isset($input['paid']) && $input['paid']==1 ? 1 : 0;
    }

    /**
     * To be used after model is created. You need to save changes manually!
     * @param Leave $model - Leave object this funciton can modify
     * @param array $input - $_POST array
     */
    public static function afterCreateLeaveType(&$model, &$input)
    {
        var_dump("Triggering afterCreateLeaveType from AutoApprove..");
    }
}