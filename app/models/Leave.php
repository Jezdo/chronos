<?php
/**
 * Model class for leaves management.
 *
 * @author Jezdimir Loncar <jezdimir.loncar@gmail.com>
 * @name Leave
 * @property integer $id
 * @property integer $user_id
 * @property integer $leave_type_id
 * @property string $start
 * @property string $end
 * @property string $note
 * @property boolean $approved
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Leave whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereLeaveTypeId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereStart($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereEnd($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereNote($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereApproved($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Leave whereUpdatedAt($value) 
 */
class Leave extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leaves';

    public function type(){
        return $this->hasOne('LeaveType', 'id', 'leave_type_id');
    }

}