<?php

require_once realpath(__DIR__.'/rule/rule.interface.php');
require_once realpath(__DIR__.'/rule/rule.abstract.php');
/**
 * Rule
 *
 * @property integer $id
 * @property string $title
 * @property boolean $active
 * @property string $handle
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rule whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Rule whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Rule whereActive($value) 
 * @method static \Illuminate\Database\Query\Builder|\Rule whereHandle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Rule whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Rule whereUpdatedAt($value) 
 */
class Rule extends \Eloquent {
	protected $fillable = [];

    /**
     * @param $handle
     * @param $method
     * @param $model
     * @param $input
     * @see RuleAbstract
     */
    public static function execute($handle, $method, &$model, &$input){

        // Execute rule
        RuleAbstract::execute($handle, $method, $model, $input);

    }


    /**
     * Executes list of handles (rules IDs)
     * @param array $rulesIds - List of rules IDs
     * @param $method - Rule method we're calling
     * @param $model - Object
     * @param $input - $_POST
     * @return Object array - List of executed rules
     */
    public static function executeList($rulesIds, $method, &$model, &$input){
        // List of already executed rules
        $executedRules = [];

        foreach($rulesIds as $rule){
            $rule = Rule::find($rule['id']);
            if(is_object($rule) && !in_array($rule, $executedRules)){
                Rule::execute($rule->handle, $method, $model, $input);
                $executedRules[] = $rule;
            }
        }
        return $executedRules;
    }


    /**
     * Define m2m relationship with leave types
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function leavetypes(){
        return $this->belongsToMany('LeaveType', 'leave_type_rule')->groupBy('id')->orderBy('id');
    }
}